const express = require('express')
const app = express()
var swaggerJSDoc = require('swagger-jsdoc');
// swagger definition
var swaggerDefinition = {
  info: {
    title: 'API Documentation',
    version: '1.0.0',
    description: 'Demonstrating how to describe a RESTful API with Swagger',
  },
  host: 'localhost:8000',
  basePath: '/',
};

// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./api/routes/*.routes.js'],// pass all in array 

  };

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);
// serve swagger 
app.get('/swagger.json', function(req, res) {   
  res.setHeader('Content-Type', 'application/json');   
  res.send(swaggerSpec); 
});

const bodyParser = require('body-parser');
const router = require('./api/routes/index');

var swaggerUi = require('swagger-ui-express');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const port = process.env.port || 8000;

app.get('/', function (req, res) {
  res.send('Hello World!')
})

router(app);

app.listen(port, function () {
  console.log('Example app listening on port 8000!')
})