'use strict';

module.exports = function(app){
    var customers = require('../controllers/customer.controller');

    /**
     * @swagger
     * /api/customers:
     *   post:
     *     tags:
     *       - Customers
     *     description: Creates a new customer
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: tarun
     *         description: customer object
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/customer'
     *     responses:
     *       200:
     *         description: Successfully created
     */
    app.post('/api/customers', customers.createCustomer);

    /**
     * @swagger
     * /api/customers:
     *   get:
     *     tags:
     *       - Customers
     *     description: Returns all customers
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Return all customers.
     *         schema:
     *           $ref: '#/definitions/customers'
     */
    app.get('/api/customers', customers.findAll);

    /**
     * @swagger
     * /api/customers/{customerId}:
     *   get:
     *     tags:
     *       - Customers
     *     description: Returns a single customer based on the supplied identity.
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Returns a single customer based on the supplied identity.
     *         schema:
     *           $ref: '#/definitions/customers'
     */
    app.get('/api/customers/:customerId', customers.findById);

    /**
     * @swagger
     * /api/customers/{customerId}:
     *   put:
     *     tags: 
     *      - Customers
     *     description: Updates a single customer
     *     produces: application/json
     *     parameters:
     *       firstname: Hardev
     *       in: body
     *       description: Fields for the customer resource
     *       schema:
     *         type: array
     *         $ref: '#/definitions/customer'
     *     responses:
     *       200:
     *         description: Successfully updated
     */
    app.put('/api/customers/:customerId', customers.update);

    /**
     * @swagger
     * /api/customer/{customerId}:
     *   delete:
     *     tags:
     *       - Customers
     *     description: Deletes a single customer
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: customer's id
     *         in: path
     *         required: true
     *         type: integer
     *     responses:
     *       200:
     *         description: Successfully deleted
     */
    app.delete('/api/customers/:customerId', customers.delete);
};