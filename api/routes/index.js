'use strict';

const routes = [
    require('./customer.routes'),
    require('./products.routes')
];

module.exports = function router(app) {
    return routes.forEach((route) => {
      route(app);
    });
  };

  /**
 * Created by shahab on 10/7/15.
 */
/*'use strict';
var CustomerRoute = require('./CustomerRoute')(app);
var AdminRoute = require('./AdminRoute');
var DriverRoute = require('./DriverRoute');

var all = [].concat(CustomerRoute,AdminRoute, DriverRoute);

module.exports = all;*/

