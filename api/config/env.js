"use strict";
const env = {
    DATABASE_NAME: "eco_fashion_store",
    DATABASE_USERNAME: "root",
    DATABASE_PASSWORD: "hardev123",
    DATABASE_HOST: "localhost",
    DATABASE_PORT: 3306,
    DATABASE_DIALECT: "mysql",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};
module.exports = env;