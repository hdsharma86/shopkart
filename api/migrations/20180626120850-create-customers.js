'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('customers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      customers_gender: {
        type: Sequelize.STRING
      },
      customers_firstname: {
        type: Sequelize.STRING
      },
      customers_lastname: {
        type: Sequelize.STRING
      },
      customers_dob: {
        type: Sequelize.DATE
      },
      customers_email_address: {
        type: Sequelize.STRING
      },
      customers_default_address_id: {
        type: Sequelize.STRING
      },
      customers_telephone: {
        type: Sequelize.STRING
      },
      customers_fax: {
        type: Sequelize.STRING
      },
      customers_password: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('customers');
  }
};