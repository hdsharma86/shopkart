'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      products_quantity: {
        type: Sequelize.INTEGER
      },
      products_model: {
        type: Sequelize.STRING
      },
      products_image: {
        type: Sequelize.STRING
      },
      products_price: {
        type: Sequelize.DOUBLE
      },
      products_date_added: {
        type: Sequelize.DATE
      },
      products_last_modified: {
        type: Sequelize.DATE
      },
      products_date_available: {
        type: Sequelize.DATE
      },
      products_weight: {
        type: Sequelize.DOUBLE
      },
      products_status: {
        type: Sequelize.INTEGER
      },
      products_tax_class_id: {
        type: Sequelize.INTEGER
      },
      manufacturers_id: {
        type: Sequelize.INTEGER
      },
      products_ordered: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('products');
  }
};