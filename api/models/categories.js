'use strict';
module.exports = (sequelize, DataTypes) => {
  var categories = sequelize.define('categories', {
    categories_name: DataTypes.STRING,
    categories_image: DataTypes.STRING,
    parent_id: DataTypes.INTEGER,
    sort_order: DataTypes.INTEGER
  }, {});
  categories.associate = function(models) {
    // associations can be defined here
  };
  return categories;
};