'use strict';
module.exports = (sequelize, DataTypes) => {
  var products = sequelize.define('products', {
    products_quantity: DataTypes.INTEGER,
    products_model: DataTypes.STRING,
    products_image: DataTypes.STRING,
    products_price: DataTypes.DOUBLE,
    products_date_added: DataTypes.DATE,
    products_last_modified: DataTypes.DATE,
    products_date_available: DataTypes.DATE,
    products_weight: DataTypes.DOUBLE,
    products_status: DataTypes.INTEGER,
    products_tax_class_id: DataTypes.INTEGER,
    manufacturers_id: DataTypes.INTEGER,
    products_ordered: DataTypes.INTEGER
  }, {timestamps: false});
  products.associate = function(models) {
    // associations can be defined here
  };
  return products;
};