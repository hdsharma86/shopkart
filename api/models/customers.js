'use strict';
module.exports = (sequelize, DataTypes) => {
  var customers = sequelize.define('customers', {
    customers_gender: DataTypes.STRING,
    customers_firstname: DataTypes.STRING,
    customers_lastname: DataTypes.STRING,
    customers_dob: DataTypes.DATE,
    customers_email_address: DataTypes.STRING,
    customers_default_address_id: DataTypes.STRING,
    customers_telephone: DataTypes.STRING,
    customers_fax: DataTypes.STRING,
    customers_password: DataTypes.STRING
  }, {timestamps: false});
  customers.associate = function(models) {
    // associations can be defined here
  };
  return customers;
};