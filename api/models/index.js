'use strict';
const customers = require('./customers')(sequelize, Sequelize);
const categories = require('./categories')(sequelize, Sequelize);
const products = require('./products')(sequelize, Sequelize);

const all = [].concat(customers,categories,products);
module.exports = all;