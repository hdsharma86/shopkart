'use strict';

const db = require('../config/db.config');
const Product = db.products;

/**
 * Create a new product.
 * @param {*} req 
 * @param {*} res 
 */
exports.create = (req,res) => {
    var data = req.body;
    var date = new Date();
    var currentDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    
    Product.create({
        products_quantity: data.products_quantity,
        products_model: data.products_model,
        products_price: data.products_price,
        products_date_available: currentDate,
        products_weight: data.products_weight,
        products_status: data.products_status,
        manufacturers_id: data.manufacturers_id,
        products_ordered: data.products_ordered
    }).then(product => {
        res.json(product);
    });
};

/**
 * Function to fetch all products...
 * @param {*} req 
 * @param {*} res 
 */
exports.findAll = (req, res) => {
    Product.findAll().then(products => {
        res.json(products);
    })
};

/**
 * Function to fetch product based on the supplied identity...
 * @param {*} req 
 * @param {*} res 
 */
exports.findById = (req, res) => {
    var productId = req.params.productId;
    Product.findById(productId).then(product => {
        res.json(product);
    });
};

/**
 * Function to update product...
 * @param {*} req 
 * @param {*} res 
 */
exports.update = (req, res) => {
    var data = req.body;
    var productId = req.params.productId;
    Product.update({ 
            products_quantity: data.products_quantity,
            products_model: data.products_model,
            products_price: data.products_price,
        }, 
        { 
            where: {id: productId} 
    }).then(() => {
        res.status(200).json("Updated successfully a product with id = " + productId);
    });
};

/**
 * Delete a product...
 * @param {*} req 
 * @param {*} res 
 */
exports.delete = (req,res) => {
    var productId = req.params.productId;
    Product.destroy({
        where: {id: productId}
    }).then(() => {
        res.status(200).json("Product deleted successfully...");
    });
};